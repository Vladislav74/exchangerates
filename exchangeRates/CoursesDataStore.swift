//
//  CoursesDataStore.swift
//  exchangeRates
//
//  Created by Владислав on 05.06.16.
//  Copyright © 2016 MD software. All rights reserved.
//

import Foundation


struct CoursesData {
    var todayNominal : Float
    var todayValue : Float
    var difference : Float
    var dateValue : String
    var values : [Float]
    var dates : [String]
    var bValueIsIncreases : Bool
    var dayOfUpdate : NSDate
}

class CoursesDataStore {
    
    var coursesData = [String : CoursesData]()
    
    func addNewValutaCourseData(valutaName vn : String, data : CoursesData) {
        
        coursesData.updateValue(data, forKey: vn)
    }
    
    func updateDataForValuta(valutaName vn : String, data : CoursesData) {
        
        coursesData.updateValue(data, forKey: vn)
    }
    
    func updateStore(valutaKeys vk: [String]) {
        
        //FIXME remove here extra data for valuta
        
    }
}





